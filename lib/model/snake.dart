/// 坐标点
class Point {
  final int row;
  final int column;

  Point(this.row, this.column);

  Point.clone(Point point) : this(point.row, point.column);

  /// 是否合法
  bool get valid => row >= 0 && column >= 0;

  /// 根据基点，按指定方向上相邻的点
  static Point neighbor(Point point, Direction direction) {
    assert(point != null);
    assert(direction != null);
    int r = point.row;
    int c = point.column;
    switch (direction) {
      case Direction.left:
        --c;
        break;
      case Direction.up:
        --r;
        break;
      case Direction.right:
        ++c;
        break;
      case Direction.down:
        ++r;
        break;
    }
    return Point(r, c);
  }

  @override
  bool operator ==(other) {
    return (other is Point) &&
        other != null &&
        other.row == this.row &&
        other.column == this.column;
  }

  @override
  int get hashCode {
    return this.row.hashCode ^ this.column.hashCode;
  }

  @override
  String toString() {
    return "row:$row,column:$column";
  }


}

/// 前进方向
enum Direction { left, up, right, down }

/// 游戏的状态
enum GameState{stop,play,pause}

/// 蛇
class Snake {
  final List<Point> _points = [];
  Direction _direction;
  /// 是否正常，没有出现头与尾碰撞的情况
  bool _valid=true;


  Snake({List<Point> points, Direction direction = Direction.left}) {
    assert(points != null && points.length > 0);
    assert(direction != null);
    this._points.addAll(points);
    this._direction = direction;
  }

  /// 获取蛇的长度
  int get length => _points.length;
  /// 是否正常，没有出现头与尾碰撞的情况
  bool get valid => _valid;

  /// 获取蛇的方向
  Direction get direction => _direction;

  set direction(Direction direction) {
    assert(direction != null);
    this._direction = direction;
  }

  /// 判断某个点是否属于蛇身
  bool contains( Point point ){
    return _points.contains(point);
  }

  /// 前进
  Point forward() {
    Point _point = Point.neighbor(_points[0], _direction);
    //对即将要走的点进行验证
    _valid = _point.valid && !_points.contains(_point);
    _points
      ..removeLast()
      ..insert(0, _point);
    return _point;
  }

  /// 吃掉一个蛋
  void eat(Point point) {
    _points.insert(0, point);
  }

  bool isHead(Point point) {
    return _points.first == point;
  }
}
