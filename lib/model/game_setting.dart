import 'package:flutter/material.dart';

/// 坐标点
class GameSetting {
  /// 主题颜色
  Color themeColor = Colors.black;

  /// 速度
  double speed = 300;

  /// 网格圆角大小
  double cellRadius = 5;

  /// 蛇的颜色
  Color snakeColor = Colors.white;

  /// 食物颜色
  Color foodColor = Colors.lightGreenAccent;

  GameSetting._();

  static final GameSetting _default = GameSetting._();

  factory GameSetting() => _default;

  @override
  String toString() {
    return "themeColor=$themeColor,speed=$speed,snakeColor=$snakeColor,foodColor=$foodColor";
  }
}
