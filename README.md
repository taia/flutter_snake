# fluttersnake

一个用flutter编写的贪吃蛇游戏

## Getting Started
这是一个练手项目，用一个下午时间完成了主体功能，再加半天做了逻辑判断和设置。

项目要点涉及布局、动画、组件间的事件通知
最后可能涉及到一点点的代码架构与设计


软件截图
---
<img src="https://oscimg.oschina.net/oscnet/up-5dcf0d67ba5331fdc22a60207f9816dcab5.png" width="300" />
<img src="https://oscimg.oschina.net/oscnet/up-aad7ef023c8fffde036d50dacafa322d022.png" width="300" />
<img src="https://oscimg.oschina.net/oscnet/up-c8b8cacd181573cc42971ee6febc4e5b5bf.png" width="300" />
